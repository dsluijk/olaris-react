## Olaris

#### Quickstart guide for local development

-   Run `yarn` in terminal to install all dependencies.
-   Create a `.env.local` file and add `REACT_APP_GRAPHQL_URL=SERVERURL:PORT` to the file using your server url.
-   Run `yarn dev` to begin development.
